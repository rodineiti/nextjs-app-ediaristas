<h1 align="center">
  <a href="https://nextjs.org/">
    NEXT JS
  </a>
</h1>

<p align="center">
	<strong>Web App NextJs - TreinaWeb</strong>	 - Using Node version 14.17.0 and Typescript
</p>

Clone the repository

    git clone git@gitlab.com:rodineiti/nextjs-app-ediaristas.git

Switch to the repo folder

    cd nextjs-app-ediaristas

Install all the dependencies using npm or yarn

    npm install
    yarn install

Start the local development server using npm or yarn

    npm run dev
    yarn dev

<br />

<h1>Screen</h1>

![image](https://gitlab.com/rodineiti/nextjs-app-ediaristas/uploads/bff57788c243f7ee7b1275048e34d31d/image.png)
