import React from "react";
import { experimentalStyled as styled } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";

const SafeContainer = styled("div")`
  text-align: right;
  padding: ${({ theme }) => theme.spacing(2)} 0;
  font-size: 12px;
  background-color: ${({ theme }) => theme.palette.background.default};
  color: ${({ theme }) => theme.palette.text.secondary};
`;

const SafeEnvironment = () => {
  return (
    <Container>
      <SafeContainer>
        Ambiente Seguro <i className="twf-lock"></i>{" "}
      </SafeContainer>
    </Container>
  );
};

export default SafeEnvironment;
