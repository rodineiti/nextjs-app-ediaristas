import React from "react";
import InputMask from "react-input-mask";
import { OutlinedTextFieldProps } from "@material-ui/core";
import TextFieldInput from "ui/components/inputs/TextFieldInput";

export interface TextFieldMaskProps extends OutlinedTextFieldProps {
  mask: string;
}

const TextFieldMask: React.FC<TextFieldMaskProps> = ({
  mask,
  value,
  onChange,
  ...props
}) => {
  return (
    <InputMask mask={mask} onChange={onChange} value={value}>
      {() => {
        return <TextFieldInput {...props} />;
      }}
    </InputMask>
  );
};

export default TextFieldMask;
