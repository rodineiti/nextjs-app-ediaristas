import React from "react";
import { experimentalStyled as styled } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";

const TextFieldInput = styled(TextField)`
  .MuiInputBase-root {
    background-color: ${({ theme }) => theme.palette.grey[50]};
  }

  .MuiOutlinedInput-notchedOutline {
    border-color: ${({ theme }) => theme.palette.grey[100]};
  }
`;

export default TextFieldInput;
