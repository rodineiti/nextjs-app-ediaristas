import React from "react";
import { experimentalStyled as styled } from "@material-ui/core/styles";

const TitleContainer = styled("div")`
  text-align: center;
  margin: ${({ theme }) => theme.spacing(5) + " " + "0"};
`;

const PageTitleStyle = styled("h2")`
  margin: 0;
  color: ${({ theme }) => theme.palette.primary.main};
  font-weight: 600;
  font-size: ${({ theme }) => theme.typography.h6.fontSize};

  ${({ theme }) => theme.breakpoints.down("md")} {
    font-size: ${({ theme }) => theme.typography.body1.fontSize};
  }
`;

const PageSubTitleStyle = styled("h3")`
  margin: ${({ theme }) => theme.spacing(1.5) + " " + "0"};
  color: ${({ theme }) => theme.palette.text.primary};
  font-size: ${({ theme }) => theme.typography.body1.fontSize};
  font-weight: normal;

  ${({ theme }) => theme.breakpoints.down("md")} {
    font-size: ${({ theme }) => theme.typography.body2.fontSize};
  }
`;

interface PageTitleProps {
  title: string | JSX.Element;
  subtitle?: string | JSX.Element;
}

const PageTitle: React.FC<PageTitleProps> = ({ title, subtitle }) => {
  return (
    <TitleContainer>
      <PageTitleStyle>{title}</PageTitleStyle>
      <PageSubTitleStyle>{subtitle}</PageSubTitleStyle>
    </TitleContainer>
  );
};

export default PageTitle;
