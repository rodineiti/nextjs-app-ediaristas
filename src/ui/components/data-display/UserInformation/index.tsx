import React from "react";
import { experimentalStyled as styled } from "@material-ui/core/styles";
import { Avatar, Rating } from "@material-ui/core";

const UserContainer = styled("div")`
  display: grid;
  grid-template-columns: 60px 1fr;
  grid-template-rows: repeat(3, auto);
  grid-template-areas:
    "avatar name"
    "avatar rating"
    "avatar description";
  background-color: ${({ theme }) => theme.palette.grey[50]};
  padding: ${({ theme }) => theme.spacing(3)};
  align-items: center;
  gap: ${({ theme }) => theme.spacing(0.5) + " " + theme.spacing(2)};
`;

const Name = styled("div")`
  grid-area: name;
  font-weight: bolder;
  color: ${({ theme }) => theme.palette.text.primary};
  font-size: ${({ theme }) => theme.typography.body2.fontSize};
`;

const Description = styled("div")`
  grid-area: description;
  color: ${({ theme }) => theme.palette.text.secondary};
  font-size: ${({ theme }) => theme.typography.body2.fontSize};
`;

const AvatarFile = styled(Avatar)`
  grid-area: avatar;
  width: 100%;
  height: initial;
  aspect-ratio: 1;
`;

const RatingFile = styled(Rating)`
  grid-area: rating;
  font-size: 14px;
`;

interface UserProps {
  picture: string;
  name: string;
  rating: number;
  description?: string;
}

const UserInformation: React.FC<UserProps> = ({
  picture,
  name,
  rating,
  description,
}) => {
  return (
    <UserContainer>
      <AvatarFile src={picture}>{name[0]}</AvatarFile>
      <Name>{name}</Name>
      <RatingFile readOnly value={rating} />
      <Description>{description}</Description>
    </UserContainer>
  );
};

export default UserInformation;
