import React from "react";
import { experimentalStyled as styled } from "@material-ui/core/styles";
import {
  Button,
  Typography,
  Paper,
  Container,
  CircularProgress,
} from "@material-ui/core";
import SafeEnvironment from "ui/components/feedback/SafeEnvironment";
import PageTitle from "ui/components/data-display/PageTitle";
import UserInformation from "ui/components/data-display/UserInformation";
import TextFieldMask from "ui/components/inputs/TextFieldMask";
import useIndex from "data/hooks/pages/useIndex";

const FormElementsContainer = styled("div")`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: ${({ theme }) => theme.spacing(5)};
  max-width: 650px;
  margin: 0 auto ${({ theme }) => theme.spacing(7)};
`;

const ProfessionsPaper = styled(Paper)`
  margin: 0 auto ${({ theme }) => theme.spacing(10)};
  padding: ${({ theme }) => theme.spacing(7)};

  ${({ theme }) => theme.breakpoints.down("md")} {
    &.MuiPaper-root {
      padding: 0;
      box-shadow: none;
    }
  }
`;

const ProfessionsContainer = styled("div")`
  display: grid;
  grid-template-columns: 1fr;

  ${({ theme }) => theme.breakpoints.up("md")} {
    grid-template-columns: repeat(2, 1fr);
    gap: ${({ theme }) => theme.spacing(6)};
  }

  ${({ theme }) => theme.breakpoints.down("md")} {
    margin-left: ${({ theme }) => theme.spacing(-2)};
    margin-right: ${({ theme }) => theme.spacing(-2)};

    > :nth-of-type(odd) {
      background-color: ${({ theme }) => theme.palette.background.paper};
    }
  }
`;

export default function Home() {
  const {
    cep,
    setCEP,
    cepValid,
    getProfessions,
    error,
    list,
    listRest,
    search,
    loading,
  } = useIndex();

  return (
    <div>
      <SafeEnvironment />
      <PageTitle
        title="Conheça os profissionais"
        subtitle="Preencha seu endereço e veja todos os profissionais da sua cidade"
      />

      <Container>
        <FormElementsContainer>
          <TextFieldMask
            mask={"99.999-999"}
            label="Digite seu CEP"
            variant="outlined"
            value={cep}
            onChange={(event) => setCEP(event.target.value)}
            fullWidth
          />

          {error && <Typography color="error">{error}</Typography>}

          <Button
            variant="contained"
            color="secondary"
            sx={{ width: "200px" }}
            onClick={() => getProfessions(cep)}
            disabled={!cepValid || loading}
          >
            {loading ? <CircularProgress size={20} /> : "Buscar"}
          </Button>
        </FormElementsContainer>

        {search && list.length > 0 ? (
          <ProfessionsPaper>
            <ProfessionsContainer>
              {list.map((item, index) => (
                <UserInformation
                  key={index.toString()}
                  name={item.nome_completo}
                  picture={item.foto_usuario}
                  rating={item.reputacao}
                  description={item.cidade}
                />
              ))}
            </ProfessionsContainer>
            <Container sx={{ textAlign: "center" }}>
              {listRest > 0 && (
                <Typography sx={{ mt: 5 }}>
                  ...e mais {listRest}{" "}
                  {listRest > 1
                    ? "profissionais atendem"
                    : "profissional atende"}{" "}
                  ao seu endereço.
                </Typography>
              )}
              <Button variant="contained" color="secondary" sx={{ mt: 5 }}>
                Contratar um profissional
              </Button>
            </Container>
          </ProfessionsPaper>
        ) : (
          <Typography align="center" color="textPrimary">
            Ainda não temos nenhuma diarista disponível em sua região.
          </Typography>
        )}
      </Container>
    </div>
  );
}
